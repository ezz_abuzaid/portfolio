import Vue from 'vue'
import Router from 'vue-router'
import contactVue from '@/components/contact.vue';
import homeVue from '@/components/home.vue';
import projectDetailsVue from '@/components/projectDetails.vue';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: homeVue
    },
    {
      path: '/contact',
      component: contactVue
    },
    {
      path: '/project',
      component: projectDetailsVue
    },
  ]
})
