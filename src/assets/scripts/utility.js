export const el = el => {
    const one = document.querySelector(el);
    const many = document.querySelectorAll(el);
    if (many.length < 2) return one;
    return many;
};
export class Style {
    static setProperty(el, prop, value) {
        el.style.setProperty(prop, value);
    }
    static el(el) {
        const one = document.querySelector(el);
        const many = document.querySelectorAll(el);
        if (many.length < 2) return one;
        return many;
    };
} 