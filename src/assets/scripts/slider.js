import { el, Style } from './utility';
window.requestAnimationFrame = window.requestAnimationFrame
    || window.mozRequestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.msRequestAnimationFrame
    || function (f) { return setTimeout(f, 1000 / 60) }
window.cancelAnimationFrame = window.cancelAnimationFrame
    || window.mozCancelAnimationFrame
    || function (requestID) { clearTimeout(requestID) } //fall back
export class Slider {
    constructor({ carouesl, duration, indices = true, indicatiorWrapper }) {
        this.carouesl = carouesl;
        this.duration = duration;
        this.indicatiorWrapper = indicatiorWrapper;
        this.cloneAndSetFirstSlide();
        this.slidesCount = this.carouesl.childElementCount;
        this.sliding = this.moveSlide();
        this.interval = this.autoSlide();
        if (indices) this.initIndicator();
    }
    cloneAndSetFirstSlide() {
        const firstElement = this.carouesl.firstElementChild.cloneNode(true);;
        this.carouesl.insertBefore(firstElement, this.carouesl.lastElementChild.nextElementSibling);
    };
    initIndicator() {
        let content = '';
        for (let i = 0; i < this.slidesCount - 1; i++) {
            content += `<div class="bg-danger text-white mx-1 p-2 rounded pointer">${i + 1}</div>`
        }
        this.indicatiorWrapper.innerHTML = content;
        const indices = Array.from(this.indicatiorWrapper.children);
        indices.forEach((element, index) => {
            element.addEventListener('click', (e) => {
                Style.setProperty(element, 'background-color', 'var(--dark)')
                clearInterval(this.interval);
                this.sliding.oneByOne.bind(this)(index);
                setTimeout(this.autoSlide(), 1000);
            });
        })
    };
    moveSlide(value = 0) {
        let from = value;
        return {
            oneByOne(index) {
                from = index !== undefined ? index : from;
                Style.setProperty(this.carouesl, 'transform', `translateX(-${from}00%)`);
                if (from === this.slidesCount) {
                    Style.setProperty(this.carouesl, 'transition-duration', '0s');
                    Style.setProperty(this.carouesl, 'transform', `translateX(0)`);
                    from = 1;
                } else {
                    from++;
                    Style.setProperty(this.carouesl, 'transition-duration', '1s');
                }
            }
        }

    };
    autoSlide() {
        return setInterval(this.sliding.oneByOne.bind(this), this.duration)
    }
}

// new Slider({ carouesl: $('.slider-wrapper'), duration: 4000, indices: false })